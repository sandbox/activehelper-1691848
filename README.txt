### ABOUT

  ActiveHelper LiveHelp is a powerful Live Chat Server module for Drupal 7.1x. 
  Monitor in real time your website visitors. Know how many visitors are in the website, who referred, where are from, what keywords we reused to find the website and the visitors navigation history. Know decisive information of your website visitors patterns, conversations and agents service.

  Current Features:
      * Unlimited Domains
      * Unlimited Operators
      * Unlimited Departments
	  * Multi Language Support
      * Full UTF-8 Support
      * SSL Support
      * Chat Re-branding
      * Image for each agent
      * Multi Language WordPress admin panel
	  * Real-time Chats
      * Multiple Chat Requests
      * Auto Save Transcripts
      * Capture Visitor Data
      * Chat Queue Line
      * Chat Between Agents
      * Offline Messages
      * Disable chat module in Offline mode
      * Record Offline Messages
      * Send Transcription to the Visitor
      * Ratings
      * Transfer Chats
      * Visitors Tracking
      * Visitors Navigation History
      * Chat Invitations
      * Auto start invitation
      * Disable Invitation
      * Redirect URL offline form
      * Website Push
      * Typing status notifications
      * Pre typed Responses
      * Send Links / Images / JavaScript
      * Sound & Visual Alerts
      * Smiles Images
      * Operator Rating
      * Summarize System activity
      * Support Statistics
      * Geo Location based on IP
      * Google Analytic integration.
      * Online/Offline status images      
      * Multi Language Images
      * 27 Language Translations.
      * Support Panel desktop for Windows and MAC 
      * Support panel mobile for your iPhone/ipad 
      * Support panel mobile for your Android phone and tablet
      * Support panel mobile for your BlackBerry PlayBook
      * Customizable Look & Feel

### INSTALLING

  1. Select the module file from you local hard drive and click over the install button. 
  2. Navigate to Administration -> Modules and enable the ActiveHelper LiveHelp module.
  3. Navigate to Administration -> LiveHelp Server -> Dashboard -> Setup the LiveHelp Server.

### CREDITS

  Originally developed by ActiveHelper Inc. - http://www.activehelper.com
