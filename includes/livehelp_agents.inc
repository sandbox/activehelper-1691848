<?php

function activehelper_livehelp__agents() {
	global $activehelper_livehelp;

	$output = '<h2>' . t( 'Agents' ) . '</h2>';

	// Set the default order of the table
	if ( !isset( $_GET[ 'order' ] ) ) {
		$_GET[ 'order' ] = t( 'Username' );
	}

	if ( isset( $_GET[ 'agent_delete' ] ) ) {
		activehelper_livehelp__agents_delete( $_GET[ 'agent_delete' ] );
	}

	// Prepare table header
	$table_header = array(
		array( 'data' => t( 'ID' ), 'field' => 'id' ),
		array( 'data' => t( 'Username' ), 'field' => 'username' ),
		array( 'data' => t( 'Email' ), 'field' => 'email' ),
		array( 'data' => t( 'Department' ), 'field' => 'department' ),
		array( 'data' => t( 'Status' ), 'field' => 'status' ),
		t( 'Operations' )
	);

	// Set the query params
	/*
	  SELECT id, username, status, department, email
	  FROM drupal_livehelp_users
	  ORDER BY id
	 */
	$query = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id', 'username', 'status', 'department', 'email' ) )
			// Pagination
			->extend( 'TableSort' )
			->extend( 'PagerDefault' )
			->orderByHeader( $table_header )
			->limit( $activehelper_livehelp[ 'listing_limit' ] );

	// Get the result
	$result = $query->execute();

	// Prepare table rows
	$table_rows = array( );

	// Looping for filling the table rows
	while ( $data = $result->fetchObject() ) {
		// Fill the table rows
		$table_rows[ ] = array(
			$data->id,
			$data->username,
			$data->email,
			$data->department,
			$data->status ? t( 'Enabled' ) : t( 'Disabled' ),
			l( t( 'Client info' ), 'admin/activehelper-livehelp/agents/' . $data->id . '/client-info' ) . ' &nbsp;|&nbsp; ' .
			l( t( 'Edit' ), 'admin/activehelper-livehelp/agents/' . $data->id . '/edit' ) . ' &nbsp;|&nbsp; ' .
			l(
					t( 'Delete' ), 'admin/activehelper-livehelp/agents', array(
				'attributes' => array(
					'onclick' => "return window.confirm( '" . t( 'Are you sure you want to delete the agent with username @agent?', array( '@agent' => $data->username ) ) . "' );"
				),
				'query' => array(
					'agent_delete' => $data->id
				)
					)
			)
		);
	}

	// Render the table
	$output .= theme_table( array(
				'header' => $table_header,
				'rows' => $table_rows,
				'attributes' => array( 'width' => '100%' ),
				'sticky' => true,
				'caption' => '',
				'colgroups' => array( ),
				'empty' => t( 'No agents found.' )
			) )
			. theme( 'pager' );

	return $output;
}

function activehelper_livehelp__agents_add() {
	$output = '<h2>' . t( 'Add agent' ) . '</h2>';

	$form = drupal_get_form( 'activehelper_livehelp__agents_add_edit_form', 'add' );
	$output .= render( $form );

	return $output;
}

function activehelper_livehelp__agents_edit( $agent_id = 0 ) {
	$agent_id = ( int ) $agent_id;

	$agent = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id', 'username' ) )
			->condition( 'u.id', $agent_id, '=' )
			->execute()
			->fetchAssoc();

	$output = '<h2>' . t( 'Edit agent @agent', array( '@agent' => $agent[ 'username' ] ) ) . '</h2>';

	$form = drupal_get_form( 'activehelper_livehelp__agents_add_edit_form', 'edit', $agent_id );
	$output .= render( $form );

	return $output;
}

function activehelper_livehelp__agents_add_edit_form( $form, &$form_state ) {
	global $activehelper_livehelp;

	$agent_edit = isset( $form_state[ 'build_info' ][ 'args' ][ 0 ] ) && $form_state[ 'build_info' ][ 'args' ] [ 0 ] == 'edit';

	if ( !$agent_edit ) {
		$form[ '#submit' ] = array( 'activehelper_livehelp__agents_add_form_submit' );
	}
	else {
		$form[ '#submit' ] = array( 'activehelper_livehelp__agents_edit_form_submit' );

		// Get the domain data from the DB
		$agent_id = ( int ) $form_state[ 'build_info' ][ 'args' ][ 1 ];
		$agent = db_select( 'livehelp_users', 'u' )
				->fields( 'u', array( 'id', 'username', 'status', 'department', 'email', 'firstname', 'lastname', 'privilege', 'photo' ) )
				->condition( 'u.id', $agent_id, '=' )
				->execute()
				->fetchAssoc();

		$agents_pictures_url = $activehelper_livehelp[ 'agents_pictures_url' ];
	}

	// Basic options
	$options_status = array(
		'1' => t( 'Enabled' ),
		'0' => t( 'Disabled' )
	);

	$form[ 'username' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Username' ),
		'#size' => 30,
		'#required' => true,
		'#default_value' => $agent_edit ? $agent[ 'username' ] : '',
		'#element_validate' => $agent_edit ? array( ) : array( 'activehelper_livehelp__agents_add_edit_form__username' )
	);
	$form[ 'password' ] = array(
		'#type' => 'password',
		'#title' => t( 'Password' ),
		'#description' => $agent_edit ? t( 'Only if you want to change the current password.' ) : '',
		'#size' => 20,
		'#default_value' => ''
	);
	$form[ 'email' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Email' ),
		'#size' => 30,
		'#required' => true,
		'#default_value' => $agent_edit ? $agent[ 'email' ] : ''
	);

	$form[ 'information' ] = array(
		'#type' => 'fieldset',
		'#title' => t( 'Information' ),
		'#weight' => 10,
		'#collapsible' => true,
		'#collapsed' => false
	);
	$form[ 'information' ][ 'firstname' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'First name' ),
		'#size' => 30,
		'#required' => true,
		'#default_value' => $agent_edit ? $agent[ 'firstname' ] : ''
	);
	$form[ 'information' ][ 'lastname' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Last name' ),
		'#size' => 30,
		'#required' => true,
		'#default_value' => $agent_edit ? $agent[ 'lastname' ] : ''
	);
	$form[ 'information' ][ 'department' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Department' ),
		'#size' => 30,
		'#required' => true,
		'#default_value' => $agent_edit ? $agent[ 'department' ] : ''
	);
	$form[ 'information' ][ 'status' ] = array(
		'#type' => 'radios',
		'#title' => t( 'Status' ),
		'#options' => $options_status,
		'#default_value' => $agent_edit ? $agent[ 'status' ] : 1
	);
	$form[ 'information' ][ 'privilege' ] = array(
		'#type' => 'radios',
		'#title' => t( 'Domain privilege' ),
		'#options' => $options_status,
		'#default_value' => $agent_edit ? $agent[ 'privilege' ] : 1
	);
	if ( $agent_edit && $agent[ 'photo' ] ) {
		$img = $agents_pictures_url . '/' . $agent[ 'photo' ] . '?' . rand();
	}
	$form[ 'information' ][ 'photo' ] = array(
		'#type' => 'file',
		'#title' => t( 'Picture' ),
		'#size' => 80,
		'#description' => $agent_edit && $agent[ 'photo' ] ? t( 'The already uploaded image:' ) . '<br /><br /><img src="' . $img . '" alt="" />' : ''
	);

	$form[ 'domains' ] = array(
		'#type' => 'fieldset',
		'#title' => t( 'Active domains' ),
		'#weight' => 20,
		'#collapsible' => true,
		'#collapsed' => false
	);

	// Let's pick the current domain status for this agent
	$agent_domains = array( );
	if ( $agent_edit ) {
		$query = db_select( 'livehelp_domain_user', 'du' )
				->fields( 'du', array( 'id_domain', 'status' ) )
				->condition( 'du.id_user', $agent_id, '=' );

		$result = $query->execute();
		while ( $data = $result->fetchObject() ) {
			$agent_domains[ $data->id_domain ] = $data->status;
		}
	}

	$query = db_select( 'livehelp_domains', 'd' )
			->fields( 'd', array( 'id_domain', 'name', 'status' ) );
	$result = $query->execute();

	while ( $data = $result->fetchObject() ) {
		$form[ 'domains' ][ 'domain__' . $data->id_domain ] = array(
			'#type' => 'radios',
			'#title' => $data->name,
			'#options' => $options_status,
			'#default_value' => $agent_edit ? (!empty( $agent_domains[ $data->id_domain ] ) ? 1 : 0 ) : 0
		);
	}

	$form[ 'submit' ] = array(
		'#type' => 'submit',
		'#weight' => 30,
		'#value' => t( 'Save' ),
	);

	return $form;
}

function activehelper_livehelp__agents_add_edit_form__username( $element, &$form_state ) {
	$agent = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id' ) )
			->condition( 'u.username', $element[ '#value' ], '=' )
			->execute()
			->fetchAssoc();

	if ( !empty( $agent ) ) {
		form_error( $element, t( 'The username @username is already in use.', array( '@username' => $element[ '#value' ] ) ) );
	}
}

function activehelper_livehelp__agents_add_form_submit( $form, &$form_state ) {
	global $activehelper_livehelp;

	$agents_pictures_dir = $activehelper_livehelp[ 'agents_pictures_dir' ];

	$agent_id = db_insert( 'livehelp_users' )
			->fields( array(
				'username' => $form_state[ 'values' ][ 'username' ],
				'email' => $form_state[ 'values' ][ 'email' ],
				'firstname' => $form_state[ 'values' ][ 'firstname' ],
				'lastname' => $form_state[ 'values' ][ 'lastname' ],
				'department' => $form_state[ 'values' ][ 'department' ],
				'status' => $form_state[ 'values' ][ 'status' ],
				'privilege' => $form_state[ 'values' ][ 'privilege' ],
				'datetime' => '1900-01-01 00:00:00',
				'refresh' => '1900-01-01 00:00:00',
			) )
			->execute();
	
	$form_state[ 'redirect' ] = 'admin/activehelper-livehelp/agents/' . $agent_id . '/client-info';

	foreach ( $form_state[ 'values' ] as $k => $v ) {
		if ( !empty( $v ) && strstr( $k, 'domain__' ) ) {
			$domain_id = ( int ) str_replace( 'domain__', '', $k );

			$relation_id = db_insert( 'livehelp_domain_user' )
					->fields( array(
						'id_domain' => $domain_id,
						'id_user' => $agent_id,
						'status' => 1
					) )
					->execute();

			db_insert( 'livehelp_sa_domain_user_role' )
					->fields( array(
						'id_domain_user' => $relation_id,
						'id_role' => 1
					) )
					->execute();
		}
	}

	// Let's import the images lib
	module_load_include( 'inc', 'activehelper_livehelp', 'includes/livehelp_lib_images' );
	$form_files = activehelper_livehelp__helper_multiple_files( $_FILES );
	if ( isset( $form_files[ 'files' ] ) ) {
		$form_files = $form_files[ 'files' ];
	}

	if ( isset( $form_files[ 'photo' ][ 'error' ] ) && $form_files[ 'photo' ][ 'error' ] == 0 ) {
		$agent_picture = activehelper_livehelp__lib_images_upload( DRUPAL_ROOT . '/' . $agents_pictures_dir, $agent_id, $form_files[ 'photo' ] );
		if ( !is_array( $agent_picture ) ) {
			// If $file is a string, it's an error.
			drupal_set_message( $agent_picture, 'error' );
		}
		else {
			db_query( "
				UPDATE {livehelp_users}
				SET photo = '" . $agent_picture[ 'basename' ] . "'
				WHERE id = '{$agent_id}'
			" );
		}
	}

	if ( !empty( $form_state[ 'values' ][ 'password' ] ) ) {
		db_query( "
			UPDATE {livehelp_users}
			SET password = '" . md5( $form_state[ 'values' ][ 'password' ] ) . "'
			WHERE id = '{$agent_id}'
		" );
	}

	if ( $form_state[ 'values' ][ 'status' ] ) {
		drupal_set_message( t( 'The agent @agent has been registered.', array( '@agent' => $form_state[ 'values' ][ 'username' ] ) ) );
	}
	else {
		drupal_set_message( t( 'The agent @agent has been registered as disabled.', array( '@agent' => $form_state[ 'values' ][ 'username' ] ) ), 'warning' );
	}

	return $form;
}

function activehelper_livehelp__agents_edit_form_submit( $form, &$form_state ) {
	global $activehelper_livehelp;
	
	$form_state[ 'redirect' ] = 'admin/activehelper-livehelp/agents';

	$agent_id = ( int ) $form_state[ 'build_info' ][ 'args' ][ 1 ];
	$agents_pictures_dir = $activehelper_livehelp[ 'agents_pictures_dir' ];

	$agent = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id', 'photo' ) )
			->condition( 'u.id', $agent_id, '=' )
			->execute()
			->fetchAssoc();

	db_update( 'livehelp_users' )
			->fields( array(
				'username' => $form_state[ 'values' ][ 'username' ],
				'email' => $form_state[ 'values' ][ 'email' ],
				'firstname' => $form_state[ 'values' ][ 'firstname' ],
				'lastname' => $form_state[ 'values' ][ 'lastname' ],
				'department' => $form_state[ 'values' ][ 'department' ],
				'status' => $form_state[ 'values' ][ 'status' ],
				'privilege' => $form_state[ 'values' ][ 'privilege' ]
			) )
			->condition( 'id', $agent_id, '=' )
			->execute();

	db_query( "
		DELETE FROM {livehelp_sa_domain_user_role}
		WHERE id_domain_user IN (
			SELECT id_domain_user
			FROM {livehelp_domain_user}
			WHERE id_user = {$agent_id}
		)
	" );
	db_query( "
		DELETE FROM {livehelp_domain_user}
		WHERE id_user = {$agent_id}
	" );

	foreach ( $form_state[ 'values' ] as $k => $v ) {
		if ( !empty( $v ) && strstr( $k, 'domain__' ) ) {
			$domain_id = ( int ) str_replace( 'domain__', '', $k );

			$relation_id = db_insert( 'livehelp_domain_user' )
					->fields( array(
						'id_domain' => $domain_id,
						'id_user' => $agent_id,
						'status' => 1
					) )
					->execute();

			db_insert( 'livehelp_sa_domain_user_role' )
					->fields( array(
						'id_domain_user' => $relation_id,
						'id_role' => 1
					) )
					->execute();
		}
	}

	// Let's import the images lib
	module_load_include( 'inc', 'activehelper_livehelp', 'includes/livehelp_lib_images' );
	$form_files = activehelper_livehelp__helper_multiple_files( $_FILES );
	if ( isset( $form_files[ 'files' ] ) ) {
		$form_files = $form_files[ 'files' ];
	}

	if ( isset( $form_files[ 'photo' ][ 'error' ] ) && $form_files[ 'photo' ][ 'error' ] == 0 ) {
		if ( !empty( $agent[ 'photo' ] ) ) {
			module_load_include( 'inc', 'activehelper_livehelp', 'includes/livehelp_lib_files' );
			activehelper_livehelp__lib_files_delete( $agents_pictures_dir . '/' . $agent[ 'photo' ] );
		}

		$agent_picture = activehelper_livehelp__lib_images_upload( DRUPAL_ROOT . '/' . $agents_pictures_dir, $agent_id, $form_files[ 'photo' ] );
		if ( !is_array( $agent_picture ) ) {
			// If $file is a string, it's an error.
			drupal_set_message( $agent_picture, 'error' );
		}
		else {
			db_query( "
				UPDATE {livehelp_users}
				SET photo = '" . $agent_picture[ 'basename' ] . "'
				WHERE id = '{$agent_id}'
			" );
		}
	}

	if ( !empty( $form_state[ 'values' ][ 'password' ] ) ) {
		db_query( "
			UPDATE {livehelp_users}
			SET password = '" . md5( $form_state[ 'values' ][ 'password' ] ) . "'
			WHERE id = '{$agent_id}'
		" );
	}

	drupal_set_message( t( 'The agent has been updated.' ) );

	return $form;
}

function activehelper_livehelp__agents_delete( $agent_id = 0 ) {
	global $activehelper_livehelp;

	$agent_id = ( int ) $agent_id;
	$agents_pictures_dir = $activehelper_livehelp[ 'agents_pictures_dir' ];

	$agent = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id', 'username', 'status', 'department', 'email', 'firstname', 'lastname', 'privilege', 'photo' ) )
			->condition( 'u.id', $agent_id, '=' )
			->execute()
			->fetchAssoc();

	if ( empty( $agent ) ) {
		drupal_set_message( t( 'The agent with ID @id does not exist.', array( '@id' => $agent_id ) ), 'error' );
	}
	else {
		db_query( "
			DELETE FROM {livehelp_sa_domain_user_role}
			WHERE id_domain_user IN (
				SELECT id_domain_user
				FROM {livehelp_domain_user}
				WHERE id_user = {$agent_id}
			)
		" );
		db_query( "
			DELETE FROM {livehelp_domain_user}
			WHERE id_user = {$agent_id}
		" );
		db_query( "
			DELETE FROM {livehelp_users}
			WHERE id = {$agent_id}
		" );

		module_load_include( 'inc', 'activehelper_livehelp', 'includes/livehelp_lib_files' );
		if ( !empty( $agent[ 'photo' ] ) ) {
			activehelper_livehelp__lib_files_delete( $agents_pictures_dir . '/' . $agent[ 'photo' ] );
		}

		drupal_set_message( t( 'The agent @agent has been removed.', array( '@agent' => $agent[ 'username' ] ) ), 'status' );
	}
}

function activehelper_livehelp__agents_client_info( $agent_id = 0 ) {
	$agent_id = ( int ) $agent_id;

	$agent = db_select( 'livehelp_users', 'u' )
			->fields( 'u', array( 'id', 'username' ) )
			->condition( 'u.id', $agent_id, '=' )
			->execute()
			->fetchAssoc();

	$output = '<h2>' . t( 'Client info for @agent', array( '@agent' => $agent[ 'username' ] ) ) . '</h2>';

	$form = drupal_get_form( 'activehelper_livehelp__agents_client_info_form', $agent[ 'username' ] );
	$output .= render( $form );

	return $output;
}

function activehelper_livehelp__agents_client_info_form( $form, &$form_state ) {
	global $activehelper_livehelp;

	$agent_name = $form_state[ 'build_info' ][ 'args' ][ 0 ];

	$parsed_url = parse_url( $activehelper_livehelp[ 'server_url' ] );

	$client_info_server = $parsed_url[ 'scheme' ] . '://' . $parsed_url[ 'host' ];
	$client_info_server_path = str_replace( $client_info_server, '', $activehelper_livehelp[ 'server_url' ] ) . '/';
	$client_info_username = $agent_name;
	$client_info_ssl = $parsed_url[ 'scheme' ] == 'http' ? 'OFF' : 'ON';

	$form[ 'server' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Server' ),
		'#size' => 30,
		'#default_value' => $client_info_server,
		'#attributes' => array(
			'onclick' => 'this.select();',
			'readonly' => 'readonly'
		)
	);
	$form[ 'path' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Server path' ),
		'#size' => 50,
		'#default_value' => $client_info_server_path,
		'#attributes' => array(
			'onclick' => 'this.select();',
			'readonly' => 'readonly'
		)
	);
	$form[ 'account' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Account' ),
		'#size' => 30,
		'#default_value' => 'default',
		'#attributes' => array(
			'onclick' => 'this.select();',
			'readonly' => 'readonly'
		)
	);
	$form[ 'login' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Login' ),
		'#size' => 30,
		'#default_value' => $client_info_username,
		'#attributes' => array(
			'onclick' => 'this.select();',
			'readonly' => 'readonly'
		)
	);
	$form[ 'ssl' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'SSL' ),
		'#size' => 30,
		'#default_value' => $client_info_ssl,
		'#attributes' => array(
			'readonly' => 'readonly',
			'onclick' => 'this.select();',
		),
	);

	$form[ 'submit' ] = array(
		'#type' => 'submit',
		'#value' => t( 'Back' ),
	);

	return $form;
}

function activehelper_livehelp__agents_client_info_form_submit( $form, &$form_state ) {
	$form_state[ 'redirect' ] = 'admin/activehelper-livehelp/agents';
	
	return $form;
}

