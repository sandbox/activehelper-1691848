<?php

/**
 * Copy the source folder and its content to destination.
 * 
 * @param string $source
 * @param string $destination 
 */
function activehelper_livehelp__lib_files_duplicate( $source, $destination ) {
	if ( is_dir( $source ) ) {
		@mkdir( $destination );
		$dir = dir( $source );

		while ( false !== ( $file = $dir->read() ) ) {
			if ( $file == '.' || $file == '..' )
				continue;

			$path = $source . '/' . $file;

			if ( is_dir( $path ) )
				activehelper_livehelp__lib_files_duplicate( $path, $destination . '/' . $file );
			else
				copy( $path, $destination . '/' . $file );
		}

		$dir->close();
	}
	else
		copy( $source, $destination );
}

/**
 * Deletes a source folder and its content.
 * 
 * @param string $source
 * @param boolean $delete_container Optional. False if you only need the source content deleted.
 * @return boolean.
 */
function activehelper_livehelp__lib_files_delete( $source, $delete_container = true ) {
	if ( is_file( $source ) ) {
		return @unlink( $source );
	}

	if ( !$dh = @opendir( $source ) ) {
		return false;
	}

	while ( false !== ( $obj = readdir( $dh ) ) ) {
		if ( $obj == '.' || $obj == '..' )
			continue;

		if ( !@unlink( $source . '/' . $obj ) )
			activehelper_livehelp__lib_files_delete( $source . '/' . $obj );
	}

	closedir( $dh );

	if ( $delete_container ) {
		@rmdir( $source );
	}
	
	return true;
}

/**
 * Compress a source folder and its content to destination.
 * 
 * @param string $source
 * @param string $destination
 * @return boolean 
 */
function activehelper_livehelp__lib_files_zip( $source, $destination ) {
	if ( extension_loaded( 'zip' ) === true ) {
		if ( file_exists( $source ) === true ) {
			$zip = new ZipArchive();

			if ( $zip->open( $destination, ZIPARCHIVE::CREATE ) === true ) {
				$source = realpath( $source );

				if ( is_dir( $source ) === true ) {
					$files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $source ), RecursiveIteratorIterator::SELF_FIRST );

					$baseSource = basename( $source ) . '/';
					$zip->addEmptyDir( $baseSource );

					foreach ( $files as $file ) {
						$file = realpath( $file );

						if ( is_dir( $file ) === true )
							$zip->addEmptyDir( $baseSource . substr( str_replace( $source, '', $file . '/' ), 1 ) );
						else if ( is_file( $file ) === true )
							$zip->addFromString( $baseSource . substr( str_replace( $source, '', $file ), 1 ), file_get_contents( $file ) );
					}
				}
				else if ( is_file( $source ) === true )
					$zip->addFromString( basename( $source ), file_get_contents( $source ) );
			}

			return $zip->close();
		}
	}

	return false;
}

function activehelper_livehelp__lib_files_unzip( $source, $destination ) {
	$zip = new ZipArchive;

	if ( $zip->open( $source ) ) {
		$zip->extractTo( $destination );
		$zip->close();

		return true;
	}

	return false;
}

