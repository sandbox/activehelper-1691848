<?php

function activehelper_livehelp__server_settings() {
	$output = '<h2>Server settings</h2>';

	$form = drupal_get_form( 'activehelper_livehelp__server_settings_form' );
	$output .= render( $form );

	return $output;
}

function activehelper_livehelp__server_settings_form( $form, &$form_state ) {
	global $activehelper_livehelp;

	$connection_timeout = 60;
	$keep_alive_timeout = 30;
	$guest_login_timeout= 60;
	$chat_refresh_rate = 6;
	$sound_alert_new_message = 1;

	include $activehelper_livehelp[ 'import_dir' ] . '/constants.php';

	$form[ 'connection_timeout' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Connection Timeout (sec)' ),
		'#default_value' => $connection_timeout
	);
	$form[ 'keep_alive_timeout' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Keep Alive Timeout (sec)' ),
		'#default_value' => $keep_alive_timeout
	);
	$form[ 'guest_login_timeout' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Guest Login Timeout (sec)' ),
		'#default_value' => $guest_login_timeout
	);
	$form[ 'chat_refresh_rate' ] = array(
		'#type' => 'textfield',
		'#title' => t( 'Chat Refresh Rate (sec)' ),
		'#default_value' => $chat_refresh_rate
	);
	$form[ 'sound_alert_new_message' ] = array(
		'#type' => 'select',
		'#title' => t( 'Sound alert when a new message arrive' ),
		'#options' => array( 0 => t( 'Disable' ), 1 => t( 'Enable' ) ),
		'#default_value' => $sound_alert_new_message
	);

	$form[ 'submit' ] = array(
		'#type' => 'submit',
		'#weight' => 30,
		'#value' => t( 'Save' ),
	);

	return $form;
}

function activehelper_livehelp__server_settings_form_submit( $form, &$form_state ) {
	global $activehelper_livehelp;

	$connection_timeout = 60;
	$keep_alive_timeout = 30;
	$guest_login_timeout= 60;
	$chat_refresh_rate = 6;
	$sound_alert_new_message = 1;

	include $activehelper_livehelp[ 'import_dir' ] . '/constants.php';

	$f = fopen( $activehelper_livehelp[ 'import_dir' ] . '/constants.php', 'r' );
	$output = fread( $f, filesize( $activehelper_livehelp[ 'import_dir' ] . '/constants.php' ) );
	fclose( $f );

	$output = str_replace( '$connection_timeout = ' . $connection_timeout . ';', '$connection_timeout = ' . $form_state[ 'values' ][ 'connection_timeout' ] . ';', $output );
	$output = str_replace( '$keep_alive_timeout = ' . $keep_alive_timeout . ';', '$keep_alive_timeout = ' . $form_state[ 'values' ][ 'keep_alive_timeout' ] . ';', $output );
	$output = str_replace( '$guest_login_timeout= ' . $guest_login_timeout . ';', '$guest_login_timeout= ' . $form_state[ 'values' ][ 'guest_login_timeout' ] . ';', $output );
	$output = str_replace( '$chat_refresh_rate = ' . $chat_refresh_rate . ';', '$chat_refresh_rate = ' . $form_state[ 'values' ][ 'chat_refresh_rate' ] . ';', $output );
	$output = str_replace( '$sound_alert_new_message = ' . $sound_alert_new_message . ';', '$sound_alert_new_message = ' . $form_state[ 'values' ][ 'sound_alert_new_message' ] . ';', $output );

	$f = fopen( $activehelper_livehelp[ 'import_dir' ] . '/constants.php', 'w' );
	fwrite( $f, $output );
	fclose( $f );

	drupal_set_message( t( 'The server settings have been updated.' ) );
			
	return $form;
}

